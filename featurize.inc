<?php

/**
 * @file
 * Includes base class and interface.
 */

/**
 * Interface FeaturizeInterface.
 */
interface FeaturizeInterface {

  /**
   * Components getter.
   *
   * @param string $feature
   *   Feature name.
   *
   * @return array
   *   Components.
   */
  public function getComponents($feature);

  /**
   * Directory getter.
   *
   * @param string $feature
   *   Feature name.
   *
   * @return string
   *   Directory.
   */
  public function getDirectory($feature);

  /**
   * Gets list of features.
   *
   * @return array
   *   List of features.
   */
  public function getFeatures();

  /**
   * Package getter.
   *
   * @param string $feature
   *   Feature name.
   *
   * @return string
   *   Package.
   */
  public function getPackage($feature);

  /**
   * Version getter.
   *
   * @param string $feature
   *   Feature name.
   *
   * @return string
   *   Version.
   */
  public function getVersion($feature);

}

/**
 * Class FeaturizeBase.
 */
abstract class FeaturizeBase implements FeaturizeInterface {
  /**
   * Base components variable.
   *
   * @var array
   */
  protected $components = array(
    'dependencies' => array(),
    'features' => array(),
  );

  /**
   * Package getter.
   *
   * @param string $feature
   *   Feature name.
   *
   * @return string
   *   Package.
   */
  public function getPackage($feature) {
    return 'Features';
  }

  /**
   * Version getter.
   *
   * @param string $feature
   *   Feature name.
   *
   * @return string
   *   Version.
   */
  public function getVersion($feature) {
    return '7.x-1.0';
  }

  /**
   * Directory getter.
   *
   * @param string $feature
   *   Feature name.
   *
   * @return string
   *   Directory.
   */
  public function getDirectory($feature) {
    return conf_path() . '/modules/featurize/' . $feature;
  }

  /**
   * Adds dependency record.
   *
   * @param string $module
   *   Module name.
   */
  public function addDependency($module) {
    $this->components['dependencies']['dependencies'][$module] = $module;
  }

  /**
   * Removes dependency record.
   *
   * @param string $module
   *   Module name.
   */
  public function removeDependency($module) {
    unset($this->components['dependencies']['dependencies'][$module]);
  }

  /**
   * Adds feature exclusion record.
   *
   * @param string $component
   *   Component.
   * @param string $name
   *   Component name.
   */
  public function addExclusion($component, $name) {
    $this->components['dependencies']['features_exclude'][$component][$name] = $name;
  }

  /**
   * Removes feature exclusion record.
   *
   * @param string $component
   *   Component.
   * @param string $name
   *   Component name.
   */
  public function removeExclusion($component, $name = NULL) {
    if (is_null($name)) {
      unset($this->components['dependencies']['features_exclude'][$component]);
    }
    else {
      unset($this->components['dependencies']['features_exclude'][$component][$name]);
    }
  }

  /**
   * Sets component.
   *
   * @param string $component
   *   Component.
   * @param string $name
   *   Component name.
   * @param bool $include
   *   Flag that indicates if we including or excluding the component.
   */
  protected function setComponent($component, $name, $include = TRUE) {
    if ($export_options = featurize_get_export_options($component)) {
      if (isset($export_options[$name])) {
        if ($include) {
          $this->components['features'][$component][$name] = $name;
        }
        else {
          unset($this->components['features'][$component][$name]);
        }
      }
    }
  }

  /**
   * Adds component.
   *
   * @param string $component
   *   Component.
   * @param string $name
   *   Component name.
   */
  public function addComponent($component, $name) {
    $this->setComponent($component, $name, TRUE);
  }

  /**
   * Removes component.
   *
   * @param string $component
   *   Component.
   * @param string $name
   *   Component name.
   */
  public function removeComponent($component, $name) {
    $this->setComponent($component, $name, FALSE);
  }

  /**
   * Sets components.
   *
   * @param string $component
   *   Component.
   * @param array $names
   *   Component names.
   * @param bool $include
   *   Flag that indicates if we including or excluding the component.
   */
  protected function setComponents($component, array $names, $include = TRUE) {
    foreach ($names as $name) {
      $this->setComponent($component, $name, $include);
    }
  }

  /**
   * Adds components.
   *
   * @param string $component
   *   Component.
   * @param array $names
   *   Component names.
   */
  public function addComponents($component, array $names) {
    $this->setComponents($component, $names, TRUE);
  }

  /**
   * Removes components.
   *
   * @param string $component
   *   Component.
   * @param array $names
   *   Component names.
   */
  public function removeComponents($component, array $names) {
    $this->setComponents($component, $names, FALSE);
  }

  /**
   * Sets components.
   *
   * @param string $component
   *   Component.
   * @param string $regex
   *   Regular expression.
   * @param bool $include
   *   Flag that indicates if we including or excluding the component.
   */
  public function setComponentsByRegex($component, $regex, $include = TRUE) {
    if ($export_options = featurize_get_export_options($component)) {
      foreach ($export_options as $name => $value) {
        if (preg_match($regex, $name) > 0) {
          $this->setComponent($component, $name, $include);
        }
      }
    }
  }

  /**
   * Adds components.
   *
   * @param string $component
   *   Component.
   * @param string $regex
   *   Regular expression.
   */
  public function addComponentsByRegex($component, $regex) {
    $this->setComponentsByRegex($component, $regex, TRUE);
  }

  /**
   * Removes components.
   *
   * @param string $component
   *   Component.
   * @param string $regex
   *   Regular expression.
   */
  public function removeComponentsByRegex($component, $regex) {
    $this->setComponentsByRegex($component, $regex, FALSE);
  }

  /**
   * Sets components.
   *
   * @param string $component
   *   Component.
   * @param bool $include
   *   Flag that indicates if we including or excluding the component.
   */
  public function setAllComponents($component, $include = TRUE) {
    if ($export_options = featurize_get_export_options($component)) {
      foreach ($export_options as $name => $value) {
        $this->setComponent($component, $name, $include);
      }
    }
  }

  /**
   * Adds components.
   *
   * @param string $component
   *   Component.
   */
  public function addAllComponents($component) {
    $this->setAllComponents($component, TRUE);
  }

  /**
   * Removes components.
   *
   * @param string $component
   *   Component.
   */
  public function removeAllComponents($component) {
    $this->setAllComponents($component, FALSE);
  }

}
