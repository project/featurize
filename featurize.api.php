<?php

/**
 * @file
 * Contains hooks descriptions and examples.
 */

/**
 * Implements hook_featurizers_info().
 */
function hook_featurizers_info() {
  return array(
    'featurize_test' => array(
      'name' => 'Featurize Test',
      'class' => 'FeaturizeTest',
    ),
  );
}

/**
 * Class FeaturizeTest.
 */
class FeaturizeTest extends FeaturizeBase {

  /**
   * Gets list of features.
   *
   * @return array
   *   List of features.
   */
  public function getFeatures() {
    return array(
      'featurize_test' => array(
        'name' => 'Featurize Test',
        'description' => 'Demonstrates Featurize Module.',
      ),
    );
  }

  /**
   * Components getter.
   *
   * @param string $feature
   *   Feature name.
   *
   * @return array
   *   Components.
   */
  public function getComponents($feature) {
    // Singular adding.
    $this->addComponent('variable', 'node_admin_theme');

    // Regex adding.
    $this->addComponentsByRegex('variable', '@^node_.*$@');

    // Adding all components.
    $this->addAllComponents('field_base');

    return $this->components;
  }

}
