<?php

/**
 * @file
 * Features module drush integration.
 */

/**
 * Implements hook_drush_command().
 */
function featurize_drush_command() {
  $items = array();

  $items['featurize'] = array(
    'description' => "Featurize: All Features.",
    'arguments' => array(),
    'options' => array(
      'enable' => "Enable the features created.",
    ),
    'drupal dependencies' => array('features'),
    'aliases' => array('fze'),
  );

  $items['featurize-list'] = array(
    'description' => "Featurize: List All Features.",
    'arguments' => array(),
    'options' => array(),
    'drupal dependencies' => array('features'),
    'aliases' => array('fze-l'),
  );

  return $items;
}

/**
 * Featurize: All Features.
 */
function drush_featurize() {
  $enable = drush_get_option('enable', FALSE);
  $features_allowed = drush_get_arguments();
  array_shift($features_allowed);
  featurize_featurize($enable, $features_allowed);
  return t('Featurize completed.');
}

/**
 * Featurize: List All Features.
 */
function drush_featurize_list() {
  featurize_featurize_list();
}
